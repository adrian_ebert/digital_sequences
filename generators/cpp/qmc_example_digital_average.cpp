#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <random>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <string>
#include <cstring>
#include <algorithm>
#include <math.h>

#include "digitalseq_b2g.hpp" // the C++ digital sequence point generator

// Function g
template <typename T>
T g(const std::vector<T>& x, T c, T b)
{
    using std::pow; using std::exp;
    T s = 0;
    for(int j = 1; j <= x.size(); ++j) s += x[j-1] * pow(j, -b);
    return exp(c * s);
}

template <typename T>
T gexact(int s, T c, T b)
{
    using std::pow; using std::expm1;
    T y = 1;
    for(int j = 1; j <= s; ++j)
        y *= expm1(c * pow(j, -b)) / (c * pow(j, -b));
    return y;
}

// Function h
template <typename T>
T h(const std::vector<T>& x, T a, T b)
{
    using std::pow;
    T y = 1;
    for(int j = 0; j < x.size(); ++j)
        y *= 1 + (2*x[j] - a) * pow(j+1, -b);
    return y;
}

template <typename T>
T hexact(int s, T a, T b)
{
    using std::pow;
    T y = 1;
    for(int j = 0; j < s; ++j)
        y *= 1 + (1 - a) * pow(j+1, -b);
    return y;
}

int main(int argc, char* argv[])
{
    using std::sqrt; using std::abs; using namespace std::chrono;
    typedef std::uint64_t       uint_t;
    typedef long double         float_t;
    typedef std::mt19937_64     rng_t;

    std::string test_function = "function2";
    unsigned int m = 18;
    unsigned int s = 100;
    std::string filename;
    std::uint32_t state = 0;
    unsigned t2 = 0;
    bool scramble = true;
    bool shift = true;
    //unsigned seed = rng_t::default_seed;
    std::vector<unsigned> seeds{5484,1978,2696,4314,7678,7947,63238,128,544,197,269,114,9878,1491,6798,1164};
    unsigned int r = seeds.size();

    // Either specify generating matrix or use Sobol generating matrix (default):
    if(argc > 1) {
        filename = argv[1];
    } else {
        filename = "../../digseq/sobolmats/sobol_Cs.col";
    }

    // Parameters for the function g
    float_t c = 1, b = 2; float_t I_exact;
    float_t g_exact = gexact<float_t>(s, c, b);

    // Parameters for the function h
    float_t a_h = 0, b_h = 3;
    float_t h_exact = hexact<float_t>(s, a_h, b_h);

    // Exact integral value:
    if (test_function == "function1") {
        I_exact = g_exact;
    } else if (test_function == "function2") {
        I_exact = h_exact;
    }

    // Load generating matrices from file:
    std::istream* isptr(&std::cin);
    std::ifstream ifs;
    if(filename.size()) {
        ifs.open(filename);
        if(!ifs.is_open()) {
            std::cerr << " :: Error: couldn't open " << filename << "\n";
            return 1;
        }
        isptr = &ifs;
    }

    std::cerr << "Loading: " << filename << std::endl;
    // Load generating matrices, scramble and shift if needed
    auto Cs = qmc::load_generating_matrices(*isptr, s, m);
    auto t  = qmc::bit_depth(Cs.begin(), Cs.end());
    if(t2 == 0) t2 = t;

    // Write results to the following file:
    std::string test = "qmc-example-average_";
    std::ostringstream m_sstring, s_sstring;
    m_sstring << m; s_sstring << s;
    std::string m_string(m_sstring.str()); std::string s_string(s_sstring.str());
    std::ofstream results; std::string file_string = "";
    for (int l = 0; l < filename.length(); l++) {
        if (filename[l] == '/') {
          file_string = "";
        } else {
          file_string += filename[l];
        }
    }
    test += test_function + "_m_" + m_string + "_s" + s_string + "_" + file_string + ".txt";
    results.open(test);
    results.precision(16);

    std::vector<float_t> Q_est(m+1,0);
    for (int i = 0; i < r; i++) {

      std::vector<std::uint64_t> shifts(s);
      std::vector<std::uint64_t> scrambled_Cs = Cs;
      std::vector<std::uint64_t> M(s * t);

      // Generate random scrambles and shifts if necessary
      if(shift || scramble)
      {
          rng_t rng(seeds[i]);
          std::generate_n(shifts.begin(), s, rng); // first generate
          if(!shift) std::fill_n(shifts.begin(), s, 0); // reset to zero if not needed
          if(scramble) {
              qmc::generate_random_linear_scramble(s, t2, t, M.begin(), rng);
              qmc::scramble(s, t, m, M.begin(), Cs.begin(), scrambled_Cs.begin());
          }
      }

      // Construct generator:
      qmc::digitalseq_b2g<float_t, uint_t> gen(s, m, scrambled_Cs.begin(), shifts.begin());
      gen.set_state(state);

      // Compute QMC formula:
      std::cout.precision(16);
      float_t Q = 0;
      for(int v = 0; v <= m; ++v) {
          for(int k = 0; k < (1 << v); k += 2, ++gen) {
              if (test_function == "function1") {
                  Q += g<float_t>(gen.x,c,b);
              } else if (test_function == "function2") {
                  Q += h<float_t>(gen.x,a_h,b_h);
              }
          }
          Q_est[v] += Q / (1 << v);
      }
    }
	std::cout << "Using digital sequence to approximate function in " << s << " dimensions\n";
    for(int v = 0; v <= m; ++v) {
        Q_est[v] /= r;
        std::cout << std::setw(4) << std::right << v + log2(seeds.size())
                  << "\t" << std::setw(20) << std::left << Q_est[v]
                  << "\t" << abs(Q_est[v] - I_exact) << "\n";
        results << v + log2(seeds.size()) << "\t" << abs(Q_est[v] - I_exact) << "\n";
    }
    results.close();
    return 0;
}
