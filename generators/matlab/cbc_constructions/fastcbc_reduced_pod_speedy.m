% function z = fastcbc_reduced_pod_speedy(p, m, s, omega, gamma, Gamma, w)
%
% inputs
%   p           prime number p, scalar, must be prime
%   m           number of points given by p^m, scalar
%   s           number of dimensions, scalar
%   omega       function handle for the varying kernel part of the
%               shift-invariant kernel function (assumed to be symmetric on [0,1])
%   gamma       product weights per dimension, vector [s x 1]
%   Gamma       order weights per dimension, vector [s x 1]
%   w           ascending reduction weights per dimension, integer vector [s x 1] 
%
% outputs
%   z           generating vector of the lattice rule, integer vector [s x 1]
%
% Example use:
%   Construct a lattice rule with n=p^m points using the reduced fast component-by-component construction
%   for the Korobov space with alpha = 2 and POD weights given by gamma = (0.7)^j, Gamma = j:
%
%   p = 2; m = 8; s = 10; omega = @(x) 2*pi^2*(x.^2-x+1/6); 
%   gamma = (0.7).^(1:s); Gamma = 1:s; w = zeros(s,1);
%   z = fastcbc_reduced_pod_speedy(p, m, s, omega, gamma, Gamma, w)

function z = fastcbc_reduced_pod_speedy(p, m, s, omega, gamma, Gamma, w)

if ~isprime(p), error('p must be prime'); end
if (m < 1), error('must have m >= 1'); end
if (w(1) < 0), error('must have w_j >= 0'); end

n = p^m;                                            % number of points n = p^m
if p == 2, g = 5; else, g = generator(p*p); end     % any generator for mod p^2 is also one for p^m (m > 2)
phi = zeros(m,1); Gamma = Gamma(:)';                % calculate Euler's totient function for i = 1,...,t
phi(1) = p-1; for k=2:m, phi(k) = p*phi(k-1); end  
phi(phi >= 2) = phi(phi >= 2)/2;                    % divide by 2 due to the symmetry w.r.t. omega

perm = zeros(phi(m),1); perm(1) = 1;                % g^i mod p^m sequence (cyclic group in generator order)
for i=2:phi(m), perm(i) = mod(perm(i-1)*g, n); end
if m~=1, sidx = [1; cumsum(phi(1:m-1))+1]; else, sidx = 1; end
eidx = sidx + phi - 1;                              % calculate start and end indices

psi0 = omega(0); psi = zeros(sum(phi),1);           % build the psi vector and its FFT
fft_psi = zeros(sum(phi),1);                        % this corresponds to the M_p^k matrices
for k=1:m
  psi(sidx(k):eidx(k)) = omega( mod(perm(1:phi(k)), p^k)/p^k );
  fft_psi(sidx(k):eidx(k)) = fft(psi(sidx(k):eidx(k)));
end

z = zeros(s,1); t = m - w(1); Y = p^w(1);
Gamma_frac = [Gamma(1),Gamma(2:s)./Gamma(1:s-1)];
q_pod = zeros(sum(phi),s+1); q_pod(:,1) = 1;
if (t > 0)
    for k=1:t
        if (p == 2 && k == 1)
            q_pod(sidx(w(1)+k):eidx(w(1)+k),2) = Gamma(1)*gamma(1)*psi([1 phi(k):-1:2]+sidx(k)-1);
        else    
            q_pod(sidx(w(1)+k):eidx(w(1)+k),2) = Gamma(1)*gamma(1)*repmat(psi([1 phi(k):-1:2]+sidx(k)-1),[p^w(1),1]);
        end
    end      
    for k=1:w(1)
        q_pod(sidx(k):eidx(k),2) = Gamma(1)*gamma(1)*psi0; 
    end
end
z(1) = mod(Y,n); z(1) = min(z(1),n-z(1));

for d=2:s
  t = m - w(d);                                     % calculate reduced dimension t
  if (t > 0)
      Y = p^w(d);                                   % calculate multiplicator Y_j
      q = sum(Gamma_frac(1:d) .* q_pod(:,1:d),2);
      if p == 2, r = 1; else, r = 2; end 
      E2 = zeros(phi(t),1); v = 1;
      for k=1:t
        if (p == 2 && k == 1)
            q_temp = sum(q(sidx(w(d)+k):eidx(w(d)+k)));
        else
            q_temp = sum(reshape(q(sidx(w(d)+k):eidx(w(d)+k)),[phi(w(d)+k)/p^w(d),p^w(d)]),2);
        end
        E2(1:phi(k)) = repmat(E2(1:v),phi(k)/v,1) + r*real(ifft( fft_psi(sidx(k):eidx(k)) .* fft(q_temp) ));             
        r = 2; v = phi(k);
      end
      [~,a] = min(E2);                              % pick the best choice for a
      z(d) = mod(perm(a), p^t);                  	% z_j = g^a in the reduced search space
      z(d) = mod(Y*z(d),n); z(d) = min(z(d),n-z(d));
      
      for l=d:-1:1                                  % update q_pod for l=d,...,1 (backward)
          for k=1:t
              u = mod(a-1, phi(k)) + 1;
              if (p == 2 && k == 1)
                  q_pod(sidx(w(d)+k):eidx(w(d)+k),l+1) = q_pod(sidx(w(d)+k):eidx(w(d)+k),l+1) + ...
                  Gamma_frac(l)*gamma(d)*psi([u:-1:1 phi(k):-1:u+1]+sidx(k)-1) .* q_pod(sidx(w(d)+k):eidx(w(d)+k),l);
              else
                  q_pod(sidx(w(d)+k):eidx(w(d)+k),l+1) = q_pod(sidx(w(d)+k):eidx(w(d)+k),l+1) + ...
                  Gamma_frac(l)*gamma(d)*repmat(psi([u:-1:1 phi(k):-1:u+1]+sidx(k)-1),[p^w(d),1]) .* q_pod(sidx(w(d)+k):eidx(w(d)+k),l);
              end    
          end
          for k=1:w(d)
              q_pod(sidx(k):eidx(k),l+1) = q_pod(sidx(k):eidx(k),l+1) + ...
              Gamma_frac(l)*gamma(d)*psi0 .* q_pod(sidx(k):eidx(k),l); 
          end
      end
  else
      break
  end
end
end