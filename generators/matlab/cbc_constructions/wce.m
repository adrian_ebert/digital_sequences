% Author: Adrian Ebert
%
% Squared worst-case error e2(z) for product weights 
%
% function E = wce(n, z, omega, gamma, beta)
%
% inputs
%   n           number of quadrature points, integer, scalar
%   z           generating vector, integer, 1 x s vector
%   omega       function handle for the varying kernel part of the
%               shift-invariant kernel function (assumed to be symmetric on [0,1])
%   gamma       weight sequence, real, 1 x s vector
%   beta        constant sequence, real, 1 x s vector
% outputs
%   E           squared worst-case error of z

function E = wce(n, z, omega, gamma, beta)

z = z(:)'; s = length(z);
if (length(gamma) < s), error('Gamma sequence is not long enough'); end
if (length(beta) < s), error('Beta sequence is not long enough'); end

C = ones(n,1); 
A = (0:n-1)'*z;
A = omega(mod(A,n)/n);

for j=1:s
    A(:,j) = beta(j)*ones(n,1) + gamma(j)*A(:,j);
    C = C.*A(:,j);
end
E = -prod(beta(1:s)) + sum(C)/n;
end