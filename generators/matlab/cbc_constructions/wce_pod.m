% Author: Adrian Ebert
%
% Squared worst-case error e2(z) for POD weights (suited for small s)
%
% function E = wce(n, z, omega, gamma, Gamma)
%
% inputs
%   n           number of quadrature points, integer, scalar
%   z           generating vector, integer, 1 x s vector
%   omega       function handle for the varying kernel part of the
%               shift-invariant kernel function (assumed to be symmetric on [0,1])
%   gamma       weight sequence, real, 1 x s vector
%   Gamma       order weights per dimension, vector 1 x s vector
% outputs
%   E           squared worst-case error of z

function E = wce_pod(n, z, omega, gamma, Gamma)

gamma = gamma(:)'; Gamma = Gamma(:)';
z = z(:)'; s = length(z); E = 0;
if (length(gamma) < s), error('gamma sequence is not long enough'); end
if (length(Gamma) < s), error('Gamma sequence is not long enough'); end

A = omega(mod((0:n-1)'*z,n)/n); 
for l=1:s
    C = zeros(n,1);
    sets = nchoosek(1:s,l);
    for i=1:size(sets,1)
        C = C + prod(gamma(sets(i,:)) .* A(:,sets(i,:)),2); 
    end
    E = E + Gamma(l)*sum(C)/n;
end    
end