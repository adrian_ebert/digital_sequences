% function z = fastcbc_ppower_pod_speedy(p, m, s, omega, gamma, Gamma)
%
% inputs
%   p           prime number p, scalar, must be prime
%   m           number of points given by p^m, scalar
%   s           number of dimensions, scalar
%   omega       function handle for the varying kernel part of the
%               shift-invariant kernel function (assumed to be symmetric on [0,1])
%   gamma       product weights per dimension, vector [s x 1]
%   Gamma       order weights per dimension, vector [s x 1]
%
% outputs
%   z           generating vector of the lattice rule, vector [s x 1]
%
% Example use:
%   Construct a lattice rule with n=p^m points using the component-by-component construction 
%   for the Korobov space with alpha = 2 and POD weights given by gamma = (0.7)^j, Gamma = j:
%
%   p = 2; m = 6; s = 10; omega = @(x) 2*pi^2*(x.^2-x+1/6); gamma = (0.7).^(1:s); Gamma = 1:s;
%   z = fastcbc_ppower_pod(p, m, s, omega, gamma, Gamma)

function z = fastcbc_ppower_pod_speedy(p, m, s, omega, gamma, Gamma)

if ~isprime(p), error('p must be prime'); end
if (m < 1), error('must have m >= 1'); end

n = p^m;                                            % number of points n = p^m
if p == 2, g = 5; else, g = generator(p*p); end
phi = zeros(m,1); Gamma = Gamma(:)';                % calculate Euler's totient function for i = 1,...,m
phi(1) = p-1; for k=2:m, phi(k) = p*phi(k-1); end   % phi = (p-1)*p^(m-1)
phi(phi >= 2) = phi(phi >= 2)/2;

perm = zeros(phi(m),1); perm(1) = 1;                % g^i mod p^m sequence (cyclic group)
for i=2:phi(m), perm(i) = mod(perm(i-1)*g, n); end
if m~=1, sidx = [1; cumsum(phi(1:m-1))+1]; else, sidx = 1; end
eidx = sidx + phi - 1;                              % start and end indices

psi = zeros(sum(phi),1);                            % build the psi vector and its FFT
fft_psi = zeros(sum(phi),1);                        % this corresponds to the M_p^l matrices
for k=1:m
  psi(sidx(k):eidx(k)) = omega( mod(perm(1:phi(k)), p^k)/p^k );
  fft_psi(sidx(k):eidx(k)) = fft(psi(sidx(k):eidx(k)));
end

q_pod = zeros(sum(phi),s+1); q_pod(:,1) = 1; 
for k=1:m
    q_pod(sidx(k):eidx(k),2) = Gamma(1)*gamma(1)*psi([1 phi(k):-1:2]+sidx(k)-1);
end
Gamma_frac = [Gamma(1),Gamma(2:s)./Gamma(1:s-1)];
z = ones(s,1); E2 = zeros(phi(m),1);

for d=2:s
  q = sum(Gamma_frac(1:d) .* q_pod(:,1:d),2);  
  if p == 2, r = 1; else, r = 2; end                % for p=2 we don't have half the group the first time
  E2(1) = 0; v = 1;                                 % number of choices from the previous iteration
  for k=1:m
    E2(1:phi(k)) = repmat(E2(1:v),phi(k)/v,1) + r*real(ifft( fft_psi(sidx(k):eidx(k)) .* fft(q(sidx(k):eidx(k))) ));
    r = 2; v = phi(k);
  end
  [~,w] = min(E2);                                  % pick the best choice for w
  z(d) = perm(w); z(d) = min(z(d),n-z(d));

  for l=d:-1:1                                      % update q_pod for l=d:1
      for k=1:m
          u = mod(w-1, phi(k)) + 1;
          q_pod(sidx(k):eidx(k),l+1) = q_pod(sidx(k):eidx(k),l+1) + ...
                                     Gamma_frac(l)*gamma(d)*psi([u:-1:1 phi(k):-1:u+1]+sidx(k)-1) .* q_pod(sidx(k):eidx(k),l);
      end
  end
end
end