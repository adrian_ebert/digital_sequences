% Author: Adrian Ebert
% Based on the fast CBC algorithm by Dirk Nuyens
%
% function [z, e2] = fastscs_p(n, z0, omega, gamma, beta)
%
% inputs
%   n           number of quadrature points, integer, scalar
%   z0          initial vector, vector [s x 1]
%   omega       function handle for the varying kernel part of the
%               shift-invariant kernel function (assumed to be symmetric on [0,1])
%   gamma       product weights per dimension, vector [s x 1]
%   beta        constant weights per dimension, vector [s x 1]
%
% outputs
%   z           generating vector of the lattice rule, vector [s x 1]
%   e2          worst-case error for each iteration, vector [s x 1]
%
% Example use:
%   Construct a lattice rule (with n points) using the successive-coordinate-search algorithm
%   based on the initial vector z0 in the Korobov space with alpha = 2, gamma = (0.7)^j, beta = 1:
%
%   n=31; s = 10; omega = inline('2*pi^2*(x.^2-x+1/6)');
%   gamma = (0.7).^(1:s)'; beta = ones(s,1); z0 = ones(s,1);
%   [z, e2] = fastscs_p(n, z0, omega, gamma, beta)

function [z, e2] = fastscs_p(n, z0, omega, gamma, beta)

if ~isprime(n), error('n must be prime'); end;

s = length(z0);                                 % number of dimensions s
m = (n-1)/2;                                    % assume the omega function is symmetric around 1/2 
z = zeros(s,1); e2 = zeros(s,1); 

g = generatorp(n);                              % calculate generator of U_n      
perm = zeros(m, 1);                             % g^i mod n sequence (cyclic group)
perm(1) = 1; for j=1:m-1, perm(j+1) = mod(perm(j)*g, n); end;
perm = min(n - perm, perm);   
psi0 = omega(0);
psi = omega(perm/n); fft_psi = fft(psi);        % build the psi vector and its FFT 

z0 = min(n - z0, z0); w0 = zeros(size(z0));     % components have to belong to first half of U_n
for i=1:s, w0(i) = find(perm == z0(i)); end;    % inverse permutation of start vector  

q0 = 1; q = ones(m,1);                          % initialize q0 and q w.r.t. the initial vector z0
for j=1:s
   q0 = (beta(j) + gamma(j)*psi0) * q0;
   q = (beta(j) + gamma(j) * psi([w0(j):-1:1 m:-1:w0(j)+1])) .* q;
end

prodbeta = prod(beta(1:s));
for j=1:s
    q0 = q0 / (beta(j) + gamma(j)*psi0);        % divide out the j-th component contribution
    q = q ./ (beta(j) + gamma(j) * psi([w0(j):-1:1 m:-1:w0(j)+1]));    
    E2 = real( ifft(fft_psi .* fft(q)) );
    [~,w] = min(E2);                            % pick the best choice for w
    z(j) = perm(w);
    q0 = (beta(j) + gamma(j)*psi0) * q0;        % update q0 and q
    q = (beta(j) + gamma(j) * psi([w:-1:1 m:-1:w+1])) .* q;
    e2(j) = -prodbeta + (q0 + 2*sum(q))/n;      % calculate the worst-case error
end
end