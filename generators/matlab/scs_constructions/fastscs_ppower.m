% function [z, e2] = fastscs_ppower(p, m, z0, omega, gamma, beta)
%
% inputs
%   p           prime number p, scalar, must be prime
%   m           number of points given by p^m, scalar
%   z0          initial vector with components u s.t. gcd(u,p^m) = 1, vector [s x 1]
%   omega       function handle for the varying kernel part of the
%               shift-invariant kernel function (assumed to be symmetric on [0,1])
%   gamma       product weights per dimension, vector [s x 1]
%   beta        constant weights per dimension, vector [s x 1]
%
% outputs
%   z           generating vector of the lattice rule, vector [s x 1]
%   e2          worst-case error for each iteration, vector [s x 1]
%
% Example use:
%   Construct a lattice rule (with p^m points) using the successive coordinate search algorithm
%   based on the initial vector z0 in the Korobov space with alpha = 2, gamma = (0.7)^j, beta = 1:
%
%   p = 2; m = 6; s = 10; omega = inline('2*pi^2*(x.^2-x+1/6)');
%   gamma = (0.7).^(1:s); beta = ones(1,s); z0 = ones(s,1);
%   [z, e2] = fastscs_ppower(p, m, z0, omega, gamma, beta)

function [z, e2] = fastscs_ppower(p, m, z0, omega, gamma, beta)

if ~isprime(p), error('p must be prime'); end
gcd_z0 = gcd(z0,p^m);
if ~isempty( gcd_z0(gcd_z0~=1) ), error('components of z0 must satisfy gcd(z0(j),p^m)=1'); end
if (m < 1), error('must have m >= 1'); end

s = length(z0); n = p^m;                            % number of dimensions s and points n = p^m
if p == 2, g = 5; else, g = generator(p*p); end     % any generator for mod p^2 is one for p^m
phi = zeros(m,1);                                   % calculate Euler's totient function for i = 1,...,m
phi(1) = p-1; for k=2:m, phi(k) = p*phi(k-1); end   % phi = (p-1)*p^(m-1)
phi(phi >= 2) = phi(phi >= 2)/2;

perm = zeros(phi(m),1); perm(1) = 1;                % g^i mod p^m sequence (cyclic group)
for i=2:phi(m), perm(i) = mod(perm(i-1)*g, n); end
if m~=1, sidx = [1; cumsum(phi(1:m-1))+1]; else, sidx = 1; end
eidx = sidx + phi - 1;                              % start and end indices

psi0 = omega(0); psi = zeros(sum(phi),1);  
fft_psi = zeros(sum(phi),1);
for k=1:m                                           % build the psi vector and its FFT          
  psi(sidx(k):eidx(k)) = omega( mod(perm(1:phi(k)), p^k)/p^k );
  fft_psi(sidx(k):eidx(k)) = fft(psi(sidx(k):eidx(k)));
end

z0(~ismember(z0,perm)) = mod( -z0(~ismember(z0,perm)), n);          % components have to belong to first half of U_n
w0 = zeros(size(z0)); for i=1:s, w0(i) = find(perm == z0(i)); end   % inverse permutation of start vector  

q0 = 1; q = ones(sum(phi),1);                       % initialize q0 and q w.r.t. the initial vector z0
for j=1:s
  q0 = (beta(j) + gamma(j)*psi0) * q0;
  for k=1:m
    u = mod(w0(j)-1, phi(k)) + 1;
    q(sidx(k):eidx(k)) = (beta(j) + gamma(j)*psi([u:-1:1 phi(k):-1:u+1]+sidx(k)-1)) .* q(sidx(k):eidx(k));
  end
end

prodbeta = prod(beta(1:s));
z = zeros(s,1); e2 = zeros(s,1); E2 = zeros(phi(m),1);
for j=1:s
  q0 = q0 / (beta(j) + gamma(j)*psi0);              % divide out the j-th component contribution
  for k=1:m                 
    u = mod(w0(j)-1, phi(k)) + 1;
    q(sidx(k):eidx(k)) = q(sidx(k):eidx(k)) ./ (beta(j) + gamma(j)*psi([u:-1:1 phi(k):-1:u+1]+sidx(k)-1));
  end
  C = beta(j) * q0;                                 % accumulation of the constant parts goes in C
  E2(1) = gamma(j) * psi0 * q0;
  if p == 2, r = 1; else, r = 2; end
  v = 1;                                            % number of choices from the previous iteration
  for k=1:m                                         % calculate error for each k
    C = C + beta(j) * r * sum(q(sidx(k):eidx(k)));
    E2(1:phi(k)) = repmat(E2(1:v), phi(k)/v, 1) ...
                 + gamma(j) * r * real(ifft( fft_psi(sidx(k):eidx(k)) .* fft(q(sidx(k):eidx(k))) ));
    r = 2; v = phi(k);
  end
  [~,w] = min(E2);                                  % pick the best choice for w
  z(j) = perm(w); z(j) = min(z(j),n-z(j));
  e2(j) = -prodbeta + (C + E2(w))/n;                % calculate the worst-case error
  q0 = (beta(j) + gamma(j)*psi0) * q0;              % update q0 and q
  for k=1:m
    u = mod(w-1, phi(k)) + 1;
    q(sidx(k):eidx(k)) = (beta(j) + gamma(j)*psi([u:-1:1 phi(k):-1:u+1]+sidx(k)-1)) .* q(sidx(k):eidx(k));
  end
end
end